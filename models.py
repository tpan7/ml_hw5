""" 
Keep model implementations in here.

This file is where you will write all of your code!
"""

import numpy as np
import random
from scipy.sparse import coo_matrix
from scipy.special import gammaln
from tqdm import tqdm


class LDA(object):
    def __init__(self, *, inference):
        self.inference = inference
        self.topic_words = None


    def fit(self, *, X, iterations):
        self.inference.inference(X=X, iterations=iterations)
        

    def predict(self, *, vocab, K):
        self.topic_words = {}
        preds = []
        for i, topic_dist in enumerate(self.inference.phi):
            topic_words = np.array(vocab)[np.argsort(topic_dist)][:-(K+1):-1]
            self.topic_words[i] = topic_words.tolist()
            preds.append('Topic {}: {}'.format(i, ' '.join(topic_words)))
        return preds


class Inference(object):
    """ 
    Abstract inference object.
    """

    def __init__(self, num_topics, num_docs, num_words, alpha, beta):
        self.num_topics = num_topics
        self.num_docs = num_docs
        self.num_words = num_words
        self.alpha = alpha
        self.beta = beta
        self.theta = np.zeros((num_docs, num_topics))
        self.phi = np.zeros((num_topics, num_words))
        self.loglikelihoods = []

    def inference(self, *, X, iterations):
        """ Fit the model.

        Args:
            X: A compressed sparse row matrix of ints with shape
                [num_docs, num_words].
            iterations: int giving number of iterations
        """
        raise NotImplementedError()


class GibbsSampling(Inference):

    def __init__(self, *, num_topics, num_docs, num_words, alpha, beta):
        super().__init__(num_topics, num_docs, num_words, alpha, beta)
        self.ndz = np.zeros((self.num_docs, self.num_topics))
        self.nzw = np.zeros((self.num_topics, self.num_words))
        self.nz = np.zeros(self.num_topics)
        self.topics = {} # this is Z in the assignment sheet


    def initialize(self, X):
        """
        Helper function to initialize z as described in Section 2.3.
        Call this function from inference.
        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_docs, num_words].
        """
        counter = 0
        for d, w, i in zip(X.row, X.col, X.data):
            self.topics[(d, w)] = counter % self.num_topics
            self.ndz[d][counter % self.num_topics] += 1
            self.nzw[counter % self.num_topics][w] += 1
            self.nz[counter % self.num_topics] += 1
            counter += 1


    def inference(self, *, X, iterations):
        """
        Sample topic assignments and use to determine document portions and
        topic-word distributions.

        Args:
            X: A compressed sparse row matrix of ints with shape
                [num_docs, num_words].
            iterations: int giving number of iterations
        """
        # TODO: Implement this!
        self.initialize(X)

        for t in range(iterations):
            for d, w, i in zip(X.row, X.col, X.data):
                z = self.topics[(d, w)]
                self.ndz[d][z] -= 1
                self.nzw[z][w] -= 1
                self.nz[z] -= 1
                post = self._conditional(d, w)
                z = np.argmax(np.random.multinomial(1, post, size=1))
                self.topics[(d, w)] = z
                self.ndz[d][z] += 1
                self.nzw[z][w] += 1
                self.nz[z] += 1

            self.loglikelihoods.append(self._loglikelihood())
        self.theta = np.divide((self.ndz + self.alpha), (np.sum(self.ndz + self.alpha, axis=1)).reshape((self.ndz.shape[0], 1)))
        self.phi = np.divide((self.nzw + self.beta), (np.sum(self.nzw + self.beta, axis=1)).reshape((self.nzw.shape[0], 1)))


    def _conditional(self, d, w):
        """
        Compute the posterior probability p(z=k|·).
        """
        numer = np.multiply(np.transpose((self.ndz[d] + self.alpha)), (self.nzw[:, w] + self.beta))
        unnorm = np.divide(numer, np.sum(self.nzw + self.beta, axis=1))
        return unnorm/np.sum(unnorm)


    def _loglikelihood(self):
        """
        Compute model likelihood: log p(w,z) = log p(w|z) + log p(z)
        You should call this method at the end of each iteration and append
        result to self.likelihoods.
        """
        ll = 0
        for k in range(self.num_topics):
            ll += np.sum(gammaln(self.nzw[k, :] + self.beta)) - gammaln(np.sum(self.nzw[k, :] + self.beta))
            ll -= self.num_words * gammaln(self.beta) - gammaln(self.num_words * self.beta)

        for d in range(self.num_docs):
            ll += np.sum(gammaln(self.ndz[d, :] + self.alpha)) - gammaln(np.sum(self.ndz[d, :] + self.alpha))
            ll -= self.num_topics * gammaln(self.alpha) - gammaln(self.num_topics * self.alpha)
        
        return ll


class SumProduct(Inference):

    def __init__(self, *, num_topics, num_docs, num_words, num_nonzero, alpha, beta):
        super().__init__(num_topics, num_docs, num_words, alpha, beta)
        self.mu_wd = np.zeros((num_nonzero, self.num_topics)) # mu_wd -- assignments for all words
        self.mu_negw_d = np.zeros((num_words, self.num_topics)) # mu_-w,d
        self.mu_w_negd = np.zeros((num_docs, self.num_topics)) # mu_w,-d
        self.mu_theta_to_z = np.zeros(self.num_topics) # message from factor node theta_d to variable node z_wd
        self.mu_phi_to_z = np.zeros(self.num_topics) # message from factor node phi_k to variable node z_wd


    def initialize(self, X):
        """
        Helper function to initialize z as described in Section 3.2.
        Call this function from inference.
        Args:
            X: A compressed sparse row matrix of floats with shape
                [num_docs, num_words].
        """
        self.mu_wd = np.random.RandomState(seed=0).rand(X.nnz, self.num_topics)
        self.mu_wd /= self.mu_wd.sum(axis=1, keepdims = True)

        idx = 0
        for d, w, x in zip(X.row, X.col, X.data):
            self.mu_negw_d[w] = x * self.mu_wd[idx]
            self.mu_w_negd[d] = x * self.mu_wd[idx]
            idx += 1

    def inference(self, *, X, iterations):
        """
        Use message-passing to for topic assignments and use to determine 
        document portions and topic-word distributions.

        Make sure to transpose phi when setting to self.phi

        Args:
            X: A compressed sparse row matrix of ints with shape
                [num_docs, num_words].
            iterations: int giving number of clustering iterations
        """
        # TODO: Implement this!
        self.initialize(X)

        sum_over_w = np.zeros((self.num_docs, self.num_topics))
        sum_over_d = np.zeros((self.num_words, self.num_topics))

        for t in range(iterations):
            idx = 0
            for d, w, x in zip(X.row, X.col, X.data):
                w_rows = X.col[X.row == d][X.col[X.row == d] != w]
                mu_negw_d_sum = np.sum(self.mu_negw_d[w_rows], axis=0)
                denom_sum = np.sum(mu_negw_d_sum + self.alpha)
                self.mu_theta_to_z = (mu_negw_d_sum + self.alpha) / denom_sum

                d_rows = X.row[X.col == w][X.row[X.col == w] != d]
                mu_w_negd_sum = np.sum(self.mu_w_negd[d_rows], axis=0)

                d_rows_allw = X.row[X.row != d]
                denom_sum = np.sum(self.mu_w_negd[d_rows_allw] + self.beta, axis=0)
                self.mu_phi_to_z = (mu_w_negd_sum + self.beta) / denom_sum

                messg_prod = np.multiply(self.mu_theta_to_z, self.mu_phi_to_z)
                self.mu_wd[idx] = messg_prod/np.sum(messg_prod)
                self.mu_negw_d[w] = x * self.mu_wd[idx]
                self.mu_w_negd[d] = x * self.mu_wd[idx]

                if t == (iterations - 1):
                    sum_over_w[d] += x * self.mu_wd[idx]
                    sum_over_d[w] += x * self.mu_wd[idx]

                idx += 1

            self.loglikelihoods.append(self._loglikelihood(X))

        self.theta = np.divide(sum_over_w + self.alpha, np.reshape(np.sum(sum_over_w + self.alpha, axis=1), (self.num_docs, 1)))
        self.phi = np.transpose(np.divide(sum_over_d + self.beta, np.sum(sum_over_d + self.beta, axis=0)))

    def _loglikelihood(self, X):
        """
        Compute model likelihood: log p(w,z) = log p(w|z) + log p(z)
        You should call this method at the end of each iteration and append
        result to self.likelihoods.
        """
        docs, words, data = X.row, X.col, X.data.astype(float)
        ll = 0
        for k in range(self.num_topics):
            for d, w in zip(docs, words):
                wrows = words[np.where(docs == d)[0]]
                drows = docs[np.where(words == w)[0]]
                ll += gammaln(data[wrows] @ self.mu_wd[wrows, k] + self.alpha) - gammaln(np.sum(data[wrows] @ self.mu_wd[wrows] + self.alpha))
                ll += gammaln(data[drows] @ self.mu_wd[drows, k] + self.beta) - gammaln(np.sum(data @ self.mu_wd + self.beta))

        return ll
